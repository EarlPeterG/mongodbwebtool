﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MongodbWebTool.CustomHelpers {
    public static class FileSize {
        public static string ConvertBytesToCommonSize(long size) {
            if (size < 1024)
                return size.ToString() + " b";

            else if (size >= 1024 && size < 1048576)
                return Math.Round(size / 1024.0, 2).ToString() + " kB";

            else if (size >= 1048576 && size < 1073741824)
                return Math.Round(size / 1048576.0, 2).ToString() + " MB";

            else
                return Math.Round(size / 1073741824.0, 2).ToString() + " GB";
        }
    }
}