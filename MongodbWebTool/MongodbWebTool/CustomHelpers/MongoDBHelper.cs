﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MongodbWebTool.CustomHelpers {
	public static class MongoDBHelper {
		public static bool CollectionExists(MongoClient client, string DatabaseName, string CollectionName) {
			var filter = new BsonDocument("name", CollectionName);
			var collections = client.GetDatabase(DatabaseName).ListCollections(new ListCollectionsOptions { Filter = filter });
			return collections.Any();
		}
	}
}