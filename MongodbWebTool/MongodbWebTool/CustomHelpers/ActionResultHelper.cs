﻿using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MongodbWebTool.CustomHelpers
{
    public static class ActionResultHelper
    {
        public static ActionResult HttpBadRequest() => new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        public static ActionResult HttpBadRequest(string message) => new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        public static ActionResult HttpBadRequest(ModelStateDictionary model) => HttpBadRequest(string.Join(" | ",
            model.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage)));

        public static ActionResult HttpOk() => new HttpStatusCodeResult(HttpStatusCode.OK);
    }
}