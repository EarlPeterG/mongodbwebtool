﻿using System.Web.Mvc;

namespace MongodbWebTool.Controllers {
	[Authorize]
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index() => View();
    }
}