﻿using System.Security.Cryptography;
using System.Text;

namespace MongodbWebTool.Models {
	public static class Cryptographic {

		private static string GetHash(HashAlgorithm algo, string data) {
			byte[] hashData = algo.ComputeHash(Encoding.Default.GetBytes(data));
			StringBuilder returnValue = new StringBuilder();
			for (int i = 0; i < hashData.Length; i++) {
				returnValue.Append(hashData[i].ToString("x2"));
			}
			return returnValue.ToString();
		}

		public static string GetStringMD5(string data) {
			return GetHash(MD5.Create(), data);
		}

		public static string GetStringSHA1(string data) {
			return GetHash(SHA1.Create(), data);
		}

		public static string GetStringSHA256(string data) {
			return GetHash(SHA256.Create(), data);
		}
	}
}