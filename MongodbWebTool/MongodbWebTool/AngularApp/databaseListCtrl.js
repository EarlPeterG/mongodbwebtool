﻿app.controller("databaseListCtrl", function ($scope, $rootScope, $location, $http) {
	/* Functions */
	$rootScope.refreshDatabaseList = function () {
		$rootScope.isDatabasesLoading = true;
		$http.get($domain + "Database/GetDatabases")
			.then(function success(response) {
				$rootScope.databases = response.data;

				$rootScope.isDatabasesLoading = false;
			}, function fail(response) {
				showError("Could not get databases! Try to refresh the page.");
			})
	}
	$scope.createDatabase = function () {
		$("#createDatabaseModal").modal();
		$scope.warningMessage = "";
	}
	function closeCreateDatabaseModal(destinationDatabase) {
		$("#createDatabaseModal").modal('hide');
		$rootScope.databases.push(destinationDatabase);
		$location.path("/database/" + destinationDatabase);
	}
	$scope.confirmCreateDatabase = function (newDatabaseName, newCollectionName, newDatabaseUsername, newDatabasePassword) {
		if ($rootScope.databases.contains(newDatabaseName)) {
			$scope.warningMessage = "Database already added.";
		}
		else if (isValidVarName(newDatabaseName)) {
			$http.get($domain + "Database/CreateDatabase", {
				params: {
					DatabaseName: newDatabaseName,
					CollectionName: newCollectionName,
					Username: newDatabaseUsername,
					Password: newDatabasePassword
				}
			})
				.then(function (response) {
					closeCreateDatabaseModal(newDatabaseName);
				}, function fail(response) {
					if (response.data === 'User already added.') {
						console.log(response);
						$scope.warningMessage = response.data;
					}
				});
		}
	}
});