﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Security;

namespace MongodbWebTool.Controllers {
	public class HomeController : Controller {
        public ActionResult Index(string from = "") {
			ViewBag.Message = "";
			ViewBag.MessageClass = "alert";

			if (from == "logout") {
				ViewBag.Message = "You have logged out.";
				ViewBag.MessageClass += " alert-warning";
				if (User.Identity.IsAuthenticated) FormsAuthentication.SignOut();
			}
			else if (from == "failed connection") {
				ViewBag.Message = "Could not connect to the server.";
				ViewBag.MessageClass += " alert-danger";
				if (User.Identity.IsAuthenticated) FormsAuthentication.SignOut();
			}
			else {
				if (User.Identity.IsAuthenticated)
					return RedirectToAction("Index", "Dashboard");
			}

			ViewBag.DefaultServer = Properties.Settings.Default.DefaultServer;
			return View();
        }

		[HttpPost]
		public ActionResult Index(string server = "", int port = 2707, string username = "", string password = "", bool rememberme = true) {
			try {
				string connection_string;
				if (!string.IsNullOrWhiteSpace(username) && string.IsNullOrWhiteSpace(password)) {
					connection_string = "mongodb://" + username + "@" + server + ":" + port;
				}
				else if (string.IsNullOrWhiteSpace(username) && string.IsNullOrWhiteSpace(password)) {
					connection_string = "mongodb://" + server + ":" + port;
				}
				else {
					connection_string = "mongodb://" + username + ":" + password + "@" + server + ":" + port;
				}

				MongoClient client = new MongoClient(connection_string);
				client.GetDatabase("admin").RunCommand((Command<BsonDocument>)"{ping:1}");

				FormsAuthentication.SetAuthCookie(connection_string, rememberme);
				return RedirectToAction("Index", "Dashboard");
			}
			catch (Exception ex) {
                ViewBag.MessageClass = "alert-danger";
                ViewBag.Message = Regex.Split(ex.Message, @"[^\w ,]")[0];
                return View();
			}
		}

		public ActionResult Logout() {
			FormsAuthentication.SignOut();
			return RedirectToAction("Index", "Home", new { from = "logout" });
		}

	}
}