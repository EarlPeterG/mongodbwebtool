﻿app.controller("collectionCtrl", function ($scope, $http, $routeParams) {
	$scope.databaseName = $routeParams["database"];
	$scope.collectionName = $routeParams["collection"];

	$scope.skip = 0;
	$scope.limit = 50;
	$scope.refreshDocumentList = function () {
		$scope.isDocumentsLoading = true;

		$http.get($domain + "Database/GetDocuments", {
			params: {
				DatabaseName: $scope.databaseName,
				CollectionName: $scope.collectionName,
				Skip: $scope.skip,
				Limit: $scope.limit
			}
		})
			.then(function success(response) {
				$scope.documents = response.data;

				$scope.isDocumentsLoading = false;
			}, function fail(response) {
				showError("Could not get documents! Try to refresh the page.");
			});
	}
	$scope.jumpNextLimit = function () {
		$scope.skip += $scope.limit;
		$scope.refreshDocumentList();
	}
    $scope.jumpPrevLimit = function () {
        if ($scope.skip - $scope.limit >= 0) $scope.skip -= $scope.limit;
        else $scope.skip = 0;

        $scope.refreshDocumentList();
    }

	$scope.getDocument = function (i) {
		if($("#pre_" + i).hasClass("hidden")) return;
		$("#pre_" + i + " pre").prepend("Loading... <i class='fa fa-spinner fa-spin'></i>");

		$.get($domain + "Database/GetDocument", {
			DatabaseName: $scope.databaseName,
			CollectionName: $scope.collectionName,
			Id: $("#a_" + i + " .id").text()
		},
		function (data) {
			try {
				var input = eval('(' + data + ')');

				$("#pre_" + i + " pre").jsonViewer(input);
			}
			catch (error) {
				$("#pre_" + i + " pre").html("WARNING: Error parsing data: " + error);
			}
		}, "text").fail(function () {
			$("#pre_" + i + " pre").html("WARNING: Could not get data.");
		});
	}
	$scope.toggleDocument = function (i) {
		if (!isNullOrWhiteSpaceStr($("#pre_" + i + " pre").html())) {
			// has content
			$("#i_" + i).toggleClass("fa-angle-down fa-angle-right");
			$("#pre_" + i).toggleClass("hidden");
			$("#pre_" + i + " pre").html("");
		}
		else {
			// has no content
			$("#pre_" + i).toggleClass("hidden");
			$("#i_" + i).toggleClass("fa-angle-down fa-angle-right");

			$scope.getDocument(i);
		}
	}

	// TODO: Improve insert/edit experience
	function showInsertEditModal(textArea) {
		$scope.validateBsonFail = false;
		$("#insertDocumentModalText").val((typeof textArea !== 'undefined') ? textArea : "")
		$scope.isInserting = false;
		$scope.insertWarningMessage = "";
		$("#insertDocumentModal").modal();
	}
	$scope.insertDocument = function () {
		$scope.isEditMode = false;
		showInsertEditModal();
	}
	$scope.editDocument = function (i) {
		$.get($domain + "Database/GetDocument", {
			DatabaseName: $scope.databaseName,
			CollectionName: $scope.collectionName,
			Id: $("#a_" + i + " .id").text(),
			T: new Date() // used for anti-cache
		}, function (data) {
			$scope.isEditMode = true;
			$scope.selectedIndex = i;
			showInsertEditModal(data);
		}).fail(function () {
			$("#pre_" + i + " pre").html("WARNING: Could not get data.");
		});
	}
	$scope.confirmInsertDocument = function () {
		if (isValidJson($scope.textArea)) {
			$scope.isInserting = true;

			var url = $domain + ($scope.isEditMode ? "Database/EditDocument" : "Database/CreateDocument");

			$http.post(url, {
				DatabaseName: $scope.databaseName,
				CollectionName: $scope.collectionName,
				Document: $("#insertDocumentModalText").val(),
				id: $("#a_" + $scope.selectedIndex + " .id").text()
			})
				.then(function success(response) {
					if (response.data == "OK") {
						$("#insertDocumentModal").modal('hide');

						if ($scope.isEditMode)
							$scope.getDocument($scope.selectedIndex);
						else
							$scope.refreshDocumentList();
					}
					else {
						$scope.insertWarningMessage = response.data;
					}
					$scope.isInserting = false;
				}, function fail(response) {
					$scope.insertWarningMessage = 'Could not insert/update document. Try refreshing the page.';
				});
		}
		else {
			$scope.validateBsonFail = true;
		}
	}

	$scope.validateBsonFail = false;
	$scope.validateBson = function () {
		$scope.validateBsonFail = !isValidJson($scope.textArea);
	}

	$scope.deleteDocument = function (i) {
		if (confirm("Are you sure you want to delete the selected document?")) {
			$http.get($domain + "Database/DeleteDocument", {
				params: {
					DatabaseName: $scope.databaseName,
					CollectionName: $scope.collectionName,
					Id: $("#a_" + i + " .id").text(),
					T: new Date() // used for anti-cache
				}
			})
				.then(function (response) {
					$scope.refreshDocumentList();
				}, function fail(response) {
					showError("Could not delete document! Try to refresh the page.");
				});
		}
	}

	/* Onload */
	$scope.refreshDocumentList();
});
