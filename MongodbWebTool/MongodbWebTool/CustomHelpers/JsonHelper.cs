﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MongodbWebTool.CustomHelpers {
	public static class JsonHelper {
        private const string INDENT_STRING = "    ";
        public static string FormatJson(string str) {
            var indent = 0;
            var quoted = false;
            var outstring = "";
            for (var i = 0; i < str.Length; i++) {
                var ch = str[i];
                switch (ch) {
                    case '{':
                    case '[':
                        outstring += ch;
                        if (!quoted) {
                            //outstring += Environment.NewLine;
                            Enumerable.Range(0, ++indent).ForEach(item => outstring += INDENT_STRING);
                        }
                        break;
                    case '}':
                    case ']':
                        if (!quoted) {
                            outstring += Environment.NewLine;
                            Enumerable.Range(0, --indent).ForEach(item => outstring += INDENT_STRING);
                        }
                        outstring += ch;
                        break;
                    case '"':
                        outstring += ch;
                        bool escaped = false;
                        var index = i;
                        while (index > 0 && str[--index] == '\\')
                            escaped = !escaped;
                        if (!escaped)
                            quoted = !quoted;
                        break;
                    case ',':
                        outstring += ch;
                        if (!quoted) {
                            outstring += Environment.NewLine;
                            Enumerable.Range(0, indent).ForEach(item => outstring += INDENT_STRING);
                        }
                        break;
                    case ':':
                        outstring += ch;
                        if (!quoted)
                            outstring += " ";
                        break;
                    default:
						if(quoted || string.IsNullOrWhiteSpace(ch.ToString()))
							outstring += ch;
                        break;
                }
            }
            return outstring;
        }
    }

    static class Extensions {
        public static void ForEach<T>(this IEnumerable<T> ie, Action<T> action) {
            foreach (var i in ie) {
                action(i);
            }
        }
    }
}