﻿using MongoDB.Bson;

namespace MongodbWebTool.Models {
	public static class BsonHelper {
		public static BsonValue GetPathValue(this BsonDocument doc, string path, char pathSeparator = '.') {
			var pathElements = path.Split(pathSeparator);
			BsonValue bsonValue = doc;

			var counter = 0;
			foreach (var elem in pathElements) {
				counter++;

				if (!bsonValue.IsBsonDocument && pathElements.Length != counter) {
					bsonValue = null;
					break;
				}

				if (!bsonValue.AsBsonDocument.Contains(elem)) {
					bsonValue = null;
					break;
				}

				bsonValue = bsonValue[elem];
			}

			return bsonValue;
		}
	}
}