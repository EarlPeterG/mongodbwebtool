﻿app.controller('databaseCtrl', function ($scope, $http, $routeParams, $location) {
	$scope.databaseName = $routeParams['database'];
	$scope.exportJsonUrl = $domain + '/Database/ExportJson';

	$scope.refreshCollectionList = function () {
		$scope.isCollectionsLoading = true;

		$http.get($domain + 'Database/GetCollections', { params: { DatabaseName: $scope.databaseName } })
			.then(function success(response) {
				$scope.collections = response.data;

				$scope.isCollectionsLoading = false;
			}, function fail(response) {
				console.log(response);
				showError('Could not get collections! Try to refresh the page.');
			});
	}

	$scope.selected = '';

	$scope.duplicate = {
		destination: {
			database: '',
			collection: ''
		},
		isBusy: false,
		warningMessage: '',

		showModal: function (collection) {
			$('#duplicateModal').modal();
			$scope.selected = collection.name;
			this.warningMessage = '';
			this.destination.collection = collection.name + '_copy';
		},
		closeModal: function () {
			$('#duplicateModal').modal('hide');
		},

		confirm: function () {
			if (!isValidVarName(this.destination.collection)) {
				this.warningMessage = 'Please type in a valid collection name (No space or special characters).';
				return;
			}

			this.isBusy = true;
			$http.get($domain + 'Database/DuplicateCollection', {
				params: {
					srcDatabase: $scope.databaseName,
					srcCollection: $scope.selected,
					destDatabase: this.destination.database,
					destCollection: this.destination.collection
				}
			})
				.then(function success(response) {
					$scope.duplicate.closeModal();

					if ($scope.databaseName === $scope.duplicate.destination.database)
						$scope.refreshCollectionList();
					else
						$location.path('/database/' + $scope.duplicate.destination.database);

					$scope.duplicate.isBusy = false;
				}, function fail(response) {
					console.log(response);
					$scope.duplicate.warningMessage = 'Could not duplicate collection. Please try again later.';
				});
		}
	}

	$scope.create = {
		newCollectionName: '',
		isBusy: false,
		warningMessage: '',

		showModal: function (collection) {
			$('#createModal').modal();
			this.warningMessage = '';
		},

		closeModal: function () {
			$('#createModal').modal('hide');
		},

		confirm: function () {
			if (isValidVarName(this.newCollectionName)) {

				this.isBusy = true;
				$http.get($domain + 'Database/CreateCollection', {
					params: {
						DatabaseName: $scope.databaseName,
						CollectionName: this.newCollectionName
					}
				})
					.then(function success(response) {
						$scope.create.closeModal();
						$scope.refreshCollectionList();

						$scope.duplicate.isBusy = false;
					}, function fail(response) {
						console.log(response);
						$scope.create.warningMessage = 'Could not duplicate collection. Please try again later.';
					});
			} else {
				$scope.create.warningMessage = 'Invalid collection name!';
			}
		}
	};

	$scope.delete = function (collection) {
		if (confirm('Are you sure you want to delete the collection ' + collection.name + '?')) {
			$http.get($domain + 'Database/DeleteCollection', { params: { DatabaseName: $scope.databaseName, CollectionName: collection.name } })
				.then(function success(response) {
					removeFromArray($scope.collections, collection);
				}, function fail(response) {
					console.log(response);
					showError('Could not delete collection. Try refreshing the page.');
				});
		}
	}

	$scope.empty = function (collection) {
		if (confirm('Are you sure you want to delete all the documents in the collection ' + collection.name + '?')) {
			$http.get($domain + 'Database/EmptyCollection', { params: { DatabaseName: $scope.databaseName, CollectionName: collection.name } })
				.then(function success(response) {
					$scope.refreshCollectionList();
				}, function fail(response) {
					console.log(response);
					showError('Could not empty collection. Try refreshing the page.');
				});
		}
	}

	$scope.export = {
		exportCsvUrl: $domain + '/Database/ExportCsv',
		options: '', // used as hidden field

		addHeaders: true,

		fields: [{ value: '_id' }],
		addField: function () {
			this.fields.push({ value: '' });
		},
		removeField: function (i) {
			removeByIndex($scope.export.fields, i);
		},

		showModal: function (collection) {
			$('#exportModal').modal();
			$scope.selected = collection.name;
		},
		closeModal: function () {
			$('#exportModal').modal('hide');
		},

		confirm: function () {
			var o = {
				fields: $scope.export.fields,
				addHeaders: $scope.export.addHeaders
			}
			this.options = JSON.stringify(o);
			this.closeModal();
		}
	};

	/* Onload */
	$scope.refreshCollectionList();
});
