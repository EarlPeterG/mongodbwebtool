﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MongodbWebTool.Models
{
    // Used in GetCollections, DeleteDatabase
    public class DatabaseRequest
    {
        [Required]
        public string DatabaseName { get; set; }
    }

    public class CreateDatabaseRequest
    {
        [Required]
        public string DatabaseName { get; set; }
        [Required]
        public string CollectionName { get; set; }
        [DefaultValue("")]
        public string Username { get; set; }
        [DefaultValue("")]
        public string Password { get; set; }
    }


    // Used in CreateCollection, EmptyCollection
    public class CollectionRequest
    {
        [Required]
        public string DatabaseName { get; set; }
        [Required]
        public string CollectionName { get; set; }
    }

    // Used in ExportJson, ExportCSV
    public class ExportCollectionRequest
    {
        [Required]
        public string DatabaseName { get; set; }
        [Required]
        public string CollectionName { get; set; }
        [DefaultValue("")]
        public string Options { get; set; }
    }
    public class DuplicateCollectionRequest
    {
        [Required]
        public string srcDatabase { get; set; }
        [Required]
        public string srcCollection { get; set; }
        [Required]
        public string destDatabase { get; set; }
        [Required]
        public string destCollection { get; set; }
    }



    public class GetDocumentsRequest
    {
        [Required]
        public string DatabaseName { get; set; }
        [Required]
        public string CollectionName { get; set; }
        [DefaultValue(0)]
        public int Skip { get; set; }
        [DefaultValue(50)]
        public int Limit { get; set; }
    }

    // Used in GetDocument
    public class DocumentRequest
    {
        [Required]
        public string DatabaseName { get; set; }
        [Required]
        public string CollectionName { get; set; }
        [Required]
        public string Id { get; set; }
    }


    public class NewDocumentRequest
    {
        [Required]
        public string DatabaseName { get; set; }
        [Required]
        public string CollectionName { get; set; }
        [Required]
        public string Document { get; set; }
    }


    public class EditDocumentRequest
    {
        [Required]
        public string DatabaseName { get; set; }
        [Required]
        public string CollectionName { get; set; }
        [Required]
        public string Document { get; set; }
        [Required]
        public string Id { get; set; }
    }
}