﻿Array.prototype.contains = Array.prototype.contains || function (obj) {
    var i, l = this.length;
    for (i = 0; i < l; i++) {
        if (this[i] == obj) return true;
    }
    return false;
};

/* MongoDB custom objects */
function ISODate(str) {
	return "ISODate(\"" + str + "\")";
}
function ObjectId(str) {
	return "ObjectId(\"" + str + "\")";
}
function NumberLong(str) {
	return "NumberLong(\"" + str + "\")";
}


/* Custom functions */
function removeFromArray(arr, item) {
	for (var i = arr.length; i--;) {
		if (arr[i] === item) {
			arr.splice(i, 1);
		}
	}
}
function removeByIndex(arr, index) {
    arr.splice(index, 1);
}

function isNullOrWhiteSpaceStr(str) {
	return str || str.trim() ? false : true;
}
function isValidVarName(str) {
	return /^[a-zA-Z0-9\_]*$/.test(str);
}
function isValidJson(str) {
	try {
		eval('(' + str + ')');
		return true;
	}
	catch (error) {
		return false;
	}
}

/* App functions */
function showError(error) {
	$("#errorAlert").show();
	$("#errorAlertContent").text(error);
}

/* Angular App */
var app = angular.module("mongoApp", ["ngRoute"]);

app.config(function ($routeProvider) {
	$routeProvider
		.when("/", {
			templateUrl: $domain + "AngularApp/Views/home.html"
		})
		.when("/database/:database", {
			templateUrl: $domain + "AngularApp/Views/database.html",
			controller: 'databaseCtrl'
		})
		.when("/database/:database/collection/:collection", {
			templateUrl: $domain + "AngularApp/Views/collection.html",
			controller: 'collectionCtrl'
		})
		.otherwise({
			templateUrl: $domain + "AngularApp/Views/404.html"
		});
});

/*
Custom directives
 */
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});