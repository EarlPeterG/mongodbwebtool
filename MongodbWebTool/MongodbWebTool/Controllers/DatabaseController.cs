﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using MongodbWebTool.CustomHelpers;
using MongodbWebTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using static MongodbWebTool.CustomHelpers.ActionResultHelper;

namespace MongodbWebTool.Controllers {
	[Authorize]
	// Ajax Functions (returns JSON or so) for the Angular app
	public class DatabaseController : Controller {
        private static MongoClient client;

        public DatabaseController() => client = new MongoClient(System.Web.HttpContext.Current.User.Identity.Name);

        #region "Get Functions"
        public ActionResult GetDatabases() {
            var databases = client.ListDatabases().ToList().Select(x => x["name"].AsString);
            return Content(databases.ToJson());
		}
		public ActionResult GetCollections(DatabaseRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);

			var database = client.GetDatabase(request.DatabaseName);
            var collections = database.ListCollections().ToList().OrderBy(col => col["name"].AsString.ToLower())
                .Select(col => new { name = col["name"].AsString, count = database.GetCollection<BsonDocument>(col["name"].AsString).Count(x => true) })
                .Where(col => !((string)col.name).StartsWith("system."));

            return Content(collections.ToJson(new JsonWriterSettings { OutputMode = JsonOutputMode.Strict }));
		}
		public ActionResult GetDocuments(GetDocumentsRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);

			var documents = client.GetDatabase(request.DatabaseName)
                .GetCollection<BsonDocument>(request.CollectionName)
                .Find(FilterDefinition<BsonDocument>.Empty).Skip(request.Skip).Limit(request.Limit).ToList();

			return Content(documents.ToJson(new JsonWriterSettings { OutputMode = JsonOutputMode.Strict }));
		}
		public ActionResult GetDocument(DocumentRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);
            var jsonSettings = new JsonWriterSettings {
                NewLineChars = Environment.NewLine,
                OutputMode = JsonOutputMode.Shell,
                Indent = true,
                IndentChars = "\t"
            };

			// Gets all documents in selected collection
			var collection = client.GetDatabase(request.DatabaseName).GetCollection<BsonDocument>(request.CollectionName);

			if (request.Id.StartsWith("ObjectId(\"") && request.Id.EndsWith("\")"))
				return Content(collection.Find(x => x["_id"] == new ObjectId(request.Id.Substring(10, 24))).First().ToJson(jsonSettings));
			else
				return Content(collection.Find(x => x["_id"] == request.Id).First().ToJson(jsonSettings));
		}
		#endregion

		#region "Delete Functions"
		public ActionResult DeleteDatabase(DatabaseRequest request) {
			client.DropDatabase(request.DatabaseName);

			return HttpOk();
		}
		public ActionResult DeleteCollection(CollectionRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);
			client.GetDatabase(request.DatabaseName).DropCollection(request.CollectionName);

			return HttpOk();
		}
		public ActionResult DeleteDocument(DocumentRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);

			// Gets all documents in selected collection
			var collection = client.GetDatabase(request.DatabaseName).GetCollection<BsonDocument>(request.CollectionName);

            var result = request.Id.StartsWith("ObjectId(\"") && request.Id.EndsWith("\")") ?
                collection.DeleteOne(x => x["_id"] == new ObjectId(request.Id.Substring(10, 24))) : collection.DeleteOne(x => x["_id"] == request.Id);

            if (result.DeletedCount > 0)
                return HttpOk();
            else
                return HttpBadRequest("No matched document found.");
		}
		#endregion

		#region "Empty Functions"
		public ActionResult EmptyCollection(CollectionRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);

            client.GetDatabase(request.DatabaseName).GetCollection<BsonDocument>(request.CollectionName).DeleteMany(x => true);

			return HttpOk();
		}
		#endregion

		#region "Create Functions"
		public ActionResult CreateDatabase(CreateDatabaseRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);

            if (!string.IsNullOrWhiteSpace(request.Username) && !string.IsNullOrWhiteSpace(request.Password)) {
                var collection = client.GetDatabase("admin").GetCollection<BsonDocument>("system.users");
                var credentials = MongoCredential.CreateMongoCRCredential(request.DatabaseName, request.Username, request.Password);

                var document = new BsonDocument {
                    ["_id"] = $"{request.DatabaseName}.{request.Username}",
                    ["user"] = request.Username,
                    ["db"] = request.DatabaseName,
                    ["credentials"] = credentials.ToBsonDocument(),
                    ["roles"] = new BsonArray {
                        new BsonDocument {
                            ["role"] = "readWrite",
                            ["db"] = request.DatabaseName
                        }
                    }
                };


                try {
                    collection.InsertOne(document);
                }
                catch (MongoDuplicateKeyException) {
                    return HttpBadRequest("User already added.");
                }
            }

            client.GetDatabase(request.DatabaseName).CreateCollection(request.CollectionName);

            return HttpOk();
		}
		public ActionResult CreateCollection(CollectionRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);
			client.GetDatabase(request.DatabaseName).CreateCollection(request.CollectionName);

			return HttpOk();
		}
		[HttpPost]
		public ActionResult CreateDocument(NewDocumentRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);
            var doc = BsonDocument.Parse(request.Document);

            // Gets all documents in selected collection
            client.GetDatabase(request.DatabaseName).GetCollection<BsonDocument>(request.CollectionName)
                .InsertOne(doc);

			return HttpOk();
		}
		#endregion

		#region "Update Functions"
		public ActionResult EditDocument(EditDocumentRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);
            var doc = BsonDocument.Parse(request.Document);

            // Gets all documents in selected collection
            var collection = client.GetDatabase(request.DatabaseName).GetCollection<BsonDocument>(request.CollectionName);

			if (request.Id.StartsWith("ObjectId(\"") && request.Id.EndsWith("\")"))
				collection.ReplaceOne(x => x["_id"] == new ObjectId(request.Id.Substring(10, 24)), doc);
			else
				collection.ReplaceOne(x => x["_id"] == request.Id, doc);

			return HttpOk();
		}
		#endregion

		#region "Export Functions"
		public ActionResult ExportJson(ExportCollectionRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);

            // Gets all documents in selected collection
            var collection = client.GetDatabase(request.DatabaseName).GetCollection<BsonDocument>(request.CollectionName);
			var documents = collection.Find(x => true).ToList();

			// Creates a JSON array of all documents
			var jsonSettings = new JsonWriterSettings();
			jsonSettings.NewLineChars = Environment.NewLine;
			jsonSettings.OutputMode = JsonOutputMode.Shell;
			jsonSettings.Indent = true;
			jsonSettings.IndentChars = "\t";

			List<string> jsons = new List<string> { };
			foreach (var document in documents) {
				jsons.Add(document.ToJson(jsonSettings));
			}

			// Format JSON with proper indenting
			string json = "[" + Environment.NewLine + string.Join("," + Environment.NewLine, jsons) + Environment.NewLine + "]";

			return File(Encoding.Default.GetBytes(json), "text/json", request.CollectionName + "_" + DateTime.Now.ToString() + ".json");
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="DatabaseName"></param>
		/// <param name="Collection"></param>
		/// <param name="options"></param>
		/// <returns></returns>
		public ActionResult ExportCsv(ExportCollectionRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);

            dynamic optionsObject = Newtonsoft.Json.JsonConvert.DeserializeObject(request.Options);
			var fields = (optionsObject.fields as Newtonsoft.Json.Linq.JArray).Select(field => (string)((dynamic)field).value).ToArray();
			bool addHeaders = optionsObject.addHeaders;

			// Gets all documents in selected collection
			var collection = client.GetDatabase(request.DatabaseName).GetCollection<BsonDocument>(request.CollectionName);
			var documents = collection.Find(x => true).ToList();

			var lines = new List<string> { };
			if (addHeaders) lines.Add(string.Join(",", fields));

			foreach (var document in documents) {
				var cols = new List<string> { };
				foreach(var field in fields) {
					var fieldValue = document.GetPathValue(field);
					var value = fieldValue == null ? "" : fieldValue.ToString();
					if (fieldValue.IsObjectId) value = $"ObjectId(\"{value}\")";

					// sanitize field
					if (value.Contains(",")) value = "\"" + value.Replace("\"", "\\\"") + "\"";

					cols.Add(value);
				}
				lines.Add(string.Join(",", cols));
			}

			var csv = string.Join(Environment.NewLine, lines);
			return File(Encoding.Default.GetBytes(csv), "text/json", request.CollectionName + "_" + DateTime.Now.ToString() + ".csv");
		}
		#endregion

		public ActionResult DuplicateCollection(DuplicateCollectionRequest request) {
            if (!ModelState.IsValid) return HttpBadRequest(ModelState);

			// Generate a new collection in the selected destination database
			var destination = client.GetDatabase(request.destDatabase);

			if (MongoDBHelper.CollectionExists(client, request.destDatabase, request.destCollection))
				return HttpBadRequest("The collection already exists in the destination database.");

			// Gets all documents in the source database
			var source = client.GetDatabase(request.srcDatabase).GetCollection<BsonDocument>(request.srcCollection).Find(x => true).ToList();

			// Insert to the new collection
			destination.CreateCollection(request.destCollection);
			if (source.Count > 0) {
				destination.GetCollection<BsonDocument>(request.destCollection).InsertMany(source);
			}

            return HttpOk();
		}
	}
} 